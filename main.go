package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	jsoniter "github.com/json-iterator/go"
	_ "github.com/lib/pq"
)

const (
	errorString = "{\"error\":\"%v\"}"
	psqlInfo    = "host=gostdoc.ru port=5432 user=indexing password=1212 dbname=indexing sslmode=disable"
)

func main() {
	http.HandleFunc("/search", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
			w.Header().Set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers")
			w.Header().Set("content-type", "application/json")
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			w.WriteHeader(200)
		} else {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("content-type", "application/json")
			w.Header().Set("Access-Control-Allow-Credentials", "true")
			handle(w, r)
		}
	})

	fmt.Println("starting to serve on port 8000...")
	err := http.ListenAndServe(":8000", nil)

	if err != nil {
		fmt.Printf("some error occurred: %v\n", err)
	}
}

func handle(w http.ResponseWriter, r *http.Request) {
	var req struct {
		Request *string `json:"request"`
	}

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		makeErrorResponse(w, err)
		return
	}
	fmt.Printf("status 200: \"%s\"\n", *req.Request)

	text := *req.Request
	text = requestNormalise(text)

	doubleWords := getDoubleWords(text)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		makeErrorResponse(w, err)
		return
	}

	query := "select lemma from double_word_lemma where double_word=$1"

	var lemma string
	var lemmas []string
	for _, dwords := range doubleWords {
		row := db.QueryRow(query, dwords)

		err := row.Scan(&lemma)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			makeErrorResponse(w, err)
			return
		}

		lemmas = append(lemmas, lemma)
	}
	// lemmas = deleteDoubles(lemmas)

	var dbresponse string
	query = "select response from lemma_response where lemma=$1"
	for _, lemma := range lemmas {
		row := db.QueryRow(query, lemma)

		err := row.Scan(&dbresponse)
		if err != nil && !errors.Is(err, sql.ErrNoRows) {
			makeErrorResponse(w, err)
			return
		}

		if dbresponse != "" {
			break
		}
	}

	var response struct {
		Response string `json:"response"`
	}

	if dbresponse == "" {
		response.Response = "Ничего не найдено"
	} else {
		response.Response = dbresponse
	}

	rawBytes, err := jsoniter.Marshal(response)
	if err != nil {
		makeErrorResponse(w, err)
		return
	}

	w.Write(rawBytes)
}

func getDoubleWords(s string) []string {
	var doubleWords []string
	words := strings.Split(s, " ")
	for i, _ := range words {
		if i == len(words)-1 {
			break
		}
		doubleWords = append(doubleWords, words[i]+" "+words[i+1])
	}
	return doubleWords
}

func requestNormalise(userRequest string) string {
	userRequest = strings.ToLower(userRequest)
	data, err := os.ReadFile("text.txt")
	if err != nil {
		fmt.Println(err)
		return "error"
	}
	words := strings.Split(string(data), ",")
	userRequest = remove(userRequest, words)
	return userRequest
}

func remove(str string, words []string) string {
	for _, val := range words {
		str = strings.ReplaceAll(str, " "+val+" ", " ")
	}
	return str
}

func makeErrorResponse(w http.ResponseWriter, err error) {
	errorResponse := fmt.Sprintf(errorString, err)
	w.WriteHeader(500)
	w.Write([]byte(errorResponse))
	fmt.Printf("status 500: error handling request: %v\n", err)
}

// func deleteDoubles(lemmas []string) []string {
// 	var massAllWords []string
// 	allKeys := make(map[string]bool)
// 	var massDistinctWords []string
// 	for _, val := range lemmas {
// 		massAllWords = append(massAllWords, strings.Split(val, " ")...)
// 	}
// 	for _, val := range massAllWords {
// 		if _, ok := allKeys[val]; ok {
// 			continue
// 		} else {
// 			allKeys[val] = true
// 			massDistinctWords = append(massDistinctWords, val)
// 		}
// 	}
// 	return massDistinctWords
// }
